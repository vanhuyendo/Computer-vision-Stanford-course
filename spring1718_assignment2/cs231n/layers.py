from builtins import range
import numpy as np


def affine_forward(x, w, b):
    """
    Computes the forward pass for an affine (fully-connected) layer.

    The input x has shape (N, d_1, ..., d_k) and contains a minibatch of N
    examples, where each example x[i] has shape (d_1, ..., d_k). We will
    reshape each input into a vector of dimension D = d_1 * ... * d_k, and
    then transform it to an output vector of dimension M.

    Inputs:
    - x: A numpy array containing input data, of shape (N, d_1, ..., d_k)
    - w: A numpy array of weights, of shape (D, M)
    - b: A numpy array of biases, of shape (M,)

    Returns a tuple of:
    - out: output, of shape (N, M)
    - cache: (x, w, b)
    """
    out = None
    ###########################################################################
    # TODO: Implement the affine forward pass. Store the result in out. You   #
    # will need to reshape the input into rows.                               #
    ###########################################################################
    D=np.prod(x.shape[1:])
    out=x.reshape(-1,D).dot(w)+b
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    cache = (x, w, b)
    return out, cache


def affine_backward(dout, cache):
    """
    Computes the backward pass for an affine layer.

    Inputs:
    - dout: Upstream derivative, of shape (N, M)
    - cache: Tuple of:
      - x: Input data, of shape (N, d_1, ... d_k)
      - w: Weights, of shape (D, M)
      - b: Biases, of shape (M,)

    Returns a tuple of:
    - dx: Gradient with respect to x, of shape (N, d1, ..., d_k)
    - dw: Gradient with respect to w, of shape (D, M)
    - db: Gradient with respect to b, of shape (M,)
    """
    x, w, b = cache
    dx, dw, db = None, None, None
    ###########################################################################
    # TODO: Implement the affine backward pass.                               #
    ###########################################################################
    dx=dout.dot(w.T)
    dx=dx.reshape(x.shape)
    dw=x.reshape(x.shape[0],-1).T.dot(dout)
    db=np.sum(dout,axis=0)
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return dx, dw, db


def relu_forward(x):
    """
    Computes the forward pass for a layer of rectified linear units (ReLUs).

    Input:
    - x: Inputs, of any shape

    Returns a tuple of:
    - out: Output, of the same shape as x
    - cache: x
    """
    out = None
    ###########################################################################
    # TODO: Implement the ReLU forward pass.                                  #
    ###########################################################################
    out=x.clip(0)
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    cache = x
    return out, cache


def relu_backward(dout, cache):
    """
    Computes the backward pass for a layer of rectified linear units (ReLUs).

    Input:
    - dout: Upstream derivatives, of any shape
    - cache: Input x, of same shape as dout

    Returns:
    - dx: Gradient with respect to x
    """
    dx, x = None, cache
    ###########################################################################
    # TODO: Implement the ReLU backward pass.                                 #
    ###########################################################################
    dx=x.clip(0)
    dx[dx>0]=1
    dx=dout*dx
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return dx


def batchnorm_forward(x, gamma, beta, bn_param):
    """
    Forward pass for batch normalization.

    During training the sample mean and (uncorrected) sample variance are
    computed from minibatch statistics and used to normalize the incoming data.
    During training we also keep an exponentially decaying running mean of the
    mean and variance of each feature, and these averages are used to normalize
    data at test-time.

    At each timestep we update the running averages for mean and variance using
    an exponential decay based on the momentum parameter:

    running_mean = momentum * running_mean + (1 - momentum) * sample_mean
    running_var = momentum * running_var + (1 - momentum) * sample_var

    Note that the batch normalization paper suggests a different test-time
    behavior: they compute sample mean and variance for each feature using a
    large number of training images rather than using a running average. For
    this implementation we have chosen to use running averages instead since
    they do not require an additional estimation step; the torch7
    implementation of batch normalization also uses running averages.

    Input:
    - x: Data of shape (N, D)
    - gamma: Scale parameter of shape (D,)
    - beta: Shift paremeter of shape (D,)
    - bn_param: Dictionary with the following keys:
      - mode: 'train' or 'test'; required
      - eps: Constant for numeric stability
      - momentum: Constant for running mean / variance.
      - running_mean: Array of shape (D,) giving running mean of features
      - running_var Array of shape (D,) giving running variance of features

    Returns a tuple of:
    - out: of shape (N, D)
    - cache: A tuple of values needed in the backward pass
    """
    mode = bn_param['mode']
    eps = bn_param.get('eps', 1e-5)
    momentum = bn_param.get('momentum', 0.9)

    N, D = x.shape
    running_mean = bn_param.get('running_mean', np.zeros(D, dtype=x.dtype))
    running_var = bn_param.get('running_var', np.zeros(D, dtype=x.dtype))

    out, cache = None, None
    if mode == 'train':
        #######################################################################
        # TODO: Implement the training-time forward pass for batch norm.      #
        # Use minibatch statistics to compute the mean and variance, use      #
        # these statistics to normalize the incoming data, and scale and      #
        # shift the normalized data using gamma and beta.                     #
        #                                                                     #
        # You should store the output in the variable out. Any intermediates  #
        # that you need for the backward pass should be stored in the cache   #
        # variable.                                                           #
        #                                                                     #
        # You should also use your computed sample mean and variance together #
        # with the momentum variable to update the running mean and running   #
        # variance, storing your result in the running_mean and running_var   #
        # variables.                                                          #
        #                                                                     #
        # Note that though you should be keeping track of the running         #
        # variance, you should normalize the data based on the standard       #
        # deviation (square root of variance) instead!                        # 
        # Referencing the original paper (https://arxiv.org/abs/1502.03167)   #
        # might prove to be helpful.                                          #
        #######################################################################
        x_mean=np.mean(x,axis=0)
        x_var=np.var(x, axis=0)
        x_norm=(x-x_mean)/np.sqrt(x_var+eps)
        out=x_norm*gamma+beta
        running_mean = momentum * running_mean + (1 - momentum) * x_mean
        running_var = momentum * running_var + (1 - momentum) * x_var
        cache={}
        cache["x"]=x
        cache["x_norm"]=x_norm
        cache["x_norm"]=x_norm
        cache["x_mean"]=x_mean
        cache["x_var"]=x_var
        cache["eps"]=eps
        cache["gamma"]=gamma
        #pass
        #######################################################################
        #                           END OF YOUR CODE                          #
        #######################################################################
    elif mode == 'test':
        #######################################################################
        # TODO: Implement the test-time forward pass for batch normalization. #
        # Use the running mean and variance to normalize the incoming data,   #
        # then scale and shift the normalized data using gamma and beta.      #
        # Store the result in the out variable.                               #
        #######################################################################
        x_norm=(x-running_mean)/np.sqrt(running_var+eps)
        out=x_norm*gamma+beta
        #pass
        #######################################################################
        #                          END OF YOUR CODE                           #
        #######################################################################
    else:
        raise ValueError('Invalid forward batchnorm mode "%s"' % mode)

    # Store the updated running means back into bn_param
    bn_param['running_mean'] = running_mean
    bn_param['running_var'] = running_var

    return out, cache


def batchnorm_backward(dout, cache):
    """
    Backward pass for batch normalization.

    For this implementation, you should write out a computation graph for
    batch normalization on paper and propagate gradients backward through
    intermediate nodes.

    Inputs:
    - dout: Upstream derivatives, of shape (N, D)
    - cache: Variable of intermediates from batchnorm_forward.

    Returns a tuple of:
    - dx: Gradient with respect to inputs x, of shape (N, D)
    - dgamma: Gradient with respect to scale parameter gamma, of shape (D,)
    - dbeta: Gradient with respect to shift parameter beta, of shape (D,)
    """
    dx, dgamma, dbeta = None, None, None
    ###########################################################################
    # TODO: Implement the backward pass for batch normalization. Store the    #
    # results in the dx, dgamma, and dbeta variables.                         #
    # Referencing the original paper (https://arxiv.org/abs/1502.03167)       #
    # might prove to be helpful.                                              #
    ###########################################################################
    x=cache["x"]
    x_mean=cache["x_mean"]
    x_var=cache["x_var"]
    x_norm=cache["x_norm"]
    eps=cache["eps"]
    gamma=cache["gamma"]
    N=x.shape[0]
    
    dgamma=np.sum(dout*x_norm,axis=0)
    dbeta=np.sum(dout, axis=0)
    dx_norm=dout*gamma
    #dx_ij=dx_n_ij/x_var^1/2-sum_l dx_n_lj/m*var^1/2
    #- (x_n_j/m*x_var^
    #÷~*(x_ij-x_n_j))*sum_l dx-n_lj
    dx1=dx_norm/np.sqrt(x_var+eps)
    dx2=-np.sum(dx_norm,axis=0)/(N*np.sqrt(x_var+eps))
    dx3=- (x-x_mean)*np.sum(dx_norm*x, axis=0)/(np.power(x_var+eps, 3/2)*N)
#    dx3=((x_mean*x_mean)/(np.power(x_var+eps, 3/2))*np.sum(dx_norm,axis=0))/N
    dx4=((x-x_mean)*x_mean*np.sum(dx_norm,axis=0))/(np.power(x_var+eps, 3/2)*N)
    dx=dx1+dx2+dx3+dx4
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return dx, dgamma, dbeta


def batchnorm_backward_alt(dout, cache):
    """
    Alternative backward pass for batch normalization.

    For this implementation you should work out the derivatives for the batch
    normalizaton backward pass on paper and simplify as much as possible. You
    should be able to derive a simple expression for the backward pass. 
    See the jupyter notebook for more hints.
     
    Note: This implementation should expect to receive the same cache variable
    as batchnorm_backward, but might not use all of the values in the cache.

    Inputs / outputs: Same as batchnorm_backward
    """
    dx, dgamma, dbeta = None, None, None
    ###########################################################################
    # TODO: Implement the backward pass for batch normalization. Store the    #
    # results in the dx, dgamma, and dbeta variables.                         #
    #                                                                         #
    # After computing the gradient with respect to the centered inputs, you   #
    # should be able to compute gradients with respect to the inputs in a     #
    # single statement; our implementation fits on a single 80-character line.#
    ###########################################################################
    N,D=dout.shape
    x=cache["x"]
    x_norm=cache["x_norm"]
    x_mean=cache["x_mean"]
    x_var=cache["x_var"]
    eps=cache["eps"]
    gamma=cache["gamma"]
    
    dgamma=np.sum(dout*x_norm,axis=0)
    dbeta=np.sum(dout, axis=0)
    dx_norm=dout*gamma
    
    sigma=np.sqrt(x_var + eps)
    
    dx=dx_norm*(1/sigma)+(1/N)*(1/sigma**3)*np.sum(dx_norm, axis=0)*x_mean*(x-x_mean)\
    -(1/sigma)*(1/N)*np.sum(dx_norm, axis=0)-(1/N)*(1/sigma**3)*np.sum(dx_norm*x, axis=0)*(x-x_mean)
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return dx, dgamma, dbeta


def layernorm_forward(x, gamma, beta, ln_param):
    """
    Forward pass for layer normalization.

    During both training and test-time, the incoming data is normalized per data-point,
    before being scaled by gamma and beta parameters identical to that of batch normalization.
    
    Note that in contrast to batch normalization, the behavior during train and test-time for
    layer normalization are identical, and we do not need to keep track of running averages
    of any sort.

    Input:
    - x: Data of shape (N, D)
    - gamma: Scale parameter of shape (D,)
    - beta: Shift paremeter of shape (D,)
    - ln_param: Dictionary with the following keys:
        - eps: Constant for numeric stability

    Returns a tuple of:
    - out: of shape (N, D)
    - cache: A tuple of values needed in the backward pass
    """
    out, cache = None, None
    eps = ln_param.get('eps', 1e-5)
    ###########################################################################
    # TODO: Implement the training-time forward pass for layer norm.          #
    # Normalize the incoming data, and scale and  shift the normalized data   #
    #  using gamma and beta.                                                  #
    # HINT: this can be done by slightly modifying your training-time         #
    # implementation of  batch normalization, and inserting a line or two of  #
    # well-placed code. In particular, can you think of any matrix            #
    # transformations you could perform, that would enable you to copy over   #
    # the batch norm code and leave it almost unchanged?                      #
    ###########################################################################
    x=x.T
    x_mean=np.mean(x,axis=0)
    x_var=np.var(x, axis=0)
    x_norm=(x-x_mean)/np.sqrt(x_var+eps)
    out=x_norm.T*gamma+beta
    
    cache={}
    cache["x"]=x
    cache["x_norm"]=x_norm
    cache["x_mean"]=x_mean
    cache["x_var"]=x_var
    cache["eps"]=eps
    cache["gamma"]=gamma
    
    
    
   # pass
   # use the batchnorm with a transformation 
#    bn_param['mode']='test'
#    bn_param['eps']=eps
#    out, cache = batchnorm_forward(x.T, gamma, beta, bn_param)
#    out=out.T
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return out, cache


def layernorm_backward(dout, cache):
    """
    Backward pass for layer normalization.

    For this implementation, you can heavily rely on the work you've done already
    for batch normalization.

    Inputs:
    - dout: Upstream derivatives, of shape (N, D)
    - cache: Variable of intermediates from layernorm_forward.

    Returns a tuple of:
    - dx: Gradient with respect to inputs x, of shape (N, D)
    - dgamma: Gradient with respect to scale parameter gamma, of shape (D,)
    - dbeta: Gradient with respect to shift parameter beta, of shape (D,)
    """
    dx, dgamma, dbeta = None, None, None
    ###########################################################################
    # TODO: Implement the backward pass for layer norm.                       #
    #                                                                         #
    # HINT: this can be done by slightly modifying your training-time         #
    # implementation of batch normalization. The hints to the forward pass    #
    # still apply!                                                            #
    ###########################################################################
#    dout=dout.T
    N,D=dout.shape
    x=cache["x"]
    x_norm=cache["x_norm"]
    x_mean=cache["x_mean"]
    x_var=cache["x_var"]
    eps=cache["eps"]
    gamma=cache["gamma"]
    
    dgamma=np.sum(dout*x_norm.T,axis=0)
    dbeta=np.sum(dout, axis=0)
    dx_norm=(dout*gamma).T
    
    sigma=np.sqrt(x_var + eps)
    
    dx=dx_norm*(1/sigma)+(1/D)*(1/sigma**3)*np.sum(dx_norm, axis=0)*x_mean*(x-x_mean)\
    -(1/sigma)*(1/D)*np.sum(dx_norm, axis=0)-(1/D)*(1/sigma**3)*np.sum(dx_norm*x, axis=0)*(x-x_mean)
    
    dx=dx.T
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return dx, dgamma, dbeta


def dropout_forward(x, dropout_param):
    """
    Performs the forward pass for (inverted) dropout.

    Inputs:
    - x: Input data, of any shape
    - dropout_param: A dictionary with the following keys:
      - p: Dropout parameter. We keep each neuron output with probability p.
      - mode: 'test' or 'train'. If the mode is train, then perform dropout;
        if the mode is test, then just return the input.
      - seed: Seed for the random number generator. Passing seed makes this
        function deterministic, which is needed for gradient checking but not
        in real networks.

    Outputs:
    - out: Array of the same shape as x.
    - cache: tuple (dropout_param, mask). In training mode, mask is the dropout
      mask that was used to multiply the input; in test mode, mask is None.

    NOTE: Please implement **inverted** dropout, not the vanilla version of dropout.
    See http://cs231n.github.io/neural-networks-2/#reg for more details.

    NOTE 2: Keep in mind that p is the probability of **keep** a neuron
    output; this might be contrary to some sources, where it is referred to
    as the probability of dropping a neuron output.
    """
    p, mode = dropout_param['p'], dropout_param['mode']
    if 'seed' in dropout_param:
        np.random.seed(dropout_param['seed'])
    
    mask = None
    out = None

    if mode == 'train':
        #######################################################################
        # TODO: Implement training phase forward pass for inverted dropout.   #
        # Store the dropout mask in the mask variable.                        #
        #######################################################################
        mask= (np.random.rand(*x.shape) < p) / p
        out=mask*x
        #        pass
        #######################################################################
        #                           END OF YOUR CODE                          #
        #######################################################################
    elif mode == 'test':
        #######################################################################
        # TODO: Implement the test phase forward pass for inverted dropout.   #
        #######################################################################
        out=x
#        pass
        #######################################################################
        #                            END OF YOUR CODE                         #
        #######################################################################

    cache = (dropout_param, mask)
    out = out.astype(x.dtype, copy=False)

    return out, cache


def dropout_backward(dout, cache):
    """
    Perform the backward pass for (inverted) dropout.

    Inputs:
    - dout: Upstream derivatives, of any shape
    - cache: (dropout_param, mask) from dropout_forward.
    """
    dropout_param, mask = cache
    mode = dropout_param['mode']

    dx = None
    if mode == 'train':
        #######################################################################
        # TODO: Implement training phase backward pass for inverted dropout   #
        #######################################################################
        dx=dout*mask
        pass
        #######################################################################
        #                          END OF YOUR CODE                           #
        #######################################################################
    elif mode == 'test':
        dx = dout
    return dx


def conv_forward_naive(x, w, b, conv_param):
    """
    A naive implementation of the forward pass for a convolutional layer.

    The input consists of N data points, each with C channels, height H and
    width W. We convolve each input with F different filters, where each filter
    spans all C channels and has height HH and width WW.

    Input:
    - x: Input data of shape (N, C, H, W)
    - w: Filter weights of shape (F, C, HH, WW)
    - b: Biases, of shape (F,)
    - conv_param: A dictionary with the following keys:
      - 'stride': The number of pixels between adjacent receptive fields in the
        horizontal and vertical directions.
      - 'pad': The number of pixels that will be used to zero-pad the input. 
        

    During padding, 'pad' zeros should be placed symmetrically (i.e equally on both sides)
    along the height and width axes of the input. Be careful not to modfiy the original
    input x directly.

    Returns a tuple of:
    - out: Output data, of shape (N, F, H', W') where H' and W' are given by
      H' = 1 + (H + 2 * pad - HH) / stride
      W' = 1 + (W + 2 * pad - WW) / stride
    - cache: (x, w, b, conv_param)
    """
    out = None
    ###########################################################################
    # TODO: Implement the convolutional forward pass.                         #
    # Hint: you can use the function np.pad for padding.                      #
    ###########################################################################
    N, C, H, W = x.shape
    F, C, HH, WW=w.shape
    s=conv_param["stride"]
    pad=conv_param["pad"]
    x_o=np.zeros((N,C,H+2*pad, W+pad*2))
    x_o[:,:,pad:pad+H, pad:pad+W]=x
    H_out = int(1 + (H + 2 * pad - HH) / s)
    W_out = int(1 + (W + 2 * pad - WW) / s)  
    
    out=np.zeros((N,F,H_out, W_out))

    for f in range(F):
        for n in range(N):
            for h in range(H_out):
                for ww in range(W_out):
                    out[n,f,h,ww]=np.sum(x_o[n,:,h*s:h*s+HH, ww*s:ww*s+WW]*w[f,:,:,:])
        
    out+=b.reshape(1,F,1,1)    
#    for h in range(HH):
#        for w in range(WW):
#            w_hw=w[:,:,h,w].reshape(1,F,C,1,1)
#            H_range=range(h,H+2*pad-HH+h, stride)
#            W_range=range(w,W+2*pad-WW+w,stride)
#            x_hw=x_o[:,:,H_range, W_range].reshape(N,1,C,H_out,W_out)
#            out+=w_hw*x_hw
#    out=np.sum(out, axis=2)+b        
            
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    cache = (x, w, b, conv_param)
    return out, cache


def conv_backward_naive(dout, cache):
    """
    A naive implementation of the backward pass for a convolutional layer.

    Inputs:
    - dout: Upstream derivatives.
    - cache: A tuple of (x, w, b, conv_param) as in conv_forward_naive

    Returns a tuple of:
    - dx: Gradient with respect to x
    - dw: Gradient with respect to w
    - db: Gradient with respect to b
    """
    dx, dw, db = None, None, None
    ###########################################################################
    # TODO: Implement the convolutional backward pass.                        #
    ###########################################################################
    x,w,b,conv_param=cache
    N, C, H, W = x.shape
    F, C, HH, WW=w.shape
    N, F, H1, W1=dout.shape
    
    s=conv_param["stride"]
    pad=conv_param["pad"]
    
    dx_o=np.zeros((N,C,H+2*pad, W+pad*2))
    dw=np.zeros(w.shape)
    
    
    for f in range(F):
        for n in range(N):    
            for h in range(H1):
                for ww in range(W1):
                    dx_o[n,:,h*s:h*s+HH, ww*s:(ww*s+WW)]\
                    +=dout[n,f,h,ww]*w[f,:,:,:]
   
    x_o=np.zeros((N,C,H+2*pad, W+pad*2))
    x_o[:,:,pad:pad+H, pad:pad+W]=x                
                    
    for f in range(F):
        for c in range(C):
            for h in range(H1):
                for k in range(W1):
                    dw[f,c,:,:]+=np.sum(dout[:,f,h,k].reshape(N,1,1)*x_o[:,c,h*s:h*s+HH, k*s:k*s+WW], axis=0)

    db=np.sum(dout, axis=(0,2,3))
    dx=dx_o[:,:,pad:pad+H, pad:pad+W]
    
    #transform dout to (N,F,H1*s,W1*s) where each block s*s contains the upper left and zeros for the rest.
    
#    expand_matrix=np.zeros(stride,stride)     
#    expand_matrix[0,0]=1
#    
#    expand_matrix=np.repeat(np.repeat(expand_matrix,axis=H1,axis=0).reshape(H1*stride*stride,1),W1, axis=0).reshape(1,1,H1*stride*stride,W1)
#    
#    dout_expand=(np.repeat(np.repeat(dout, stride, axis=3),stride,axis=4)).reshape(N,F,H1*stride*stride,W1)
#    
#    dout_expand=(dout_expand*expand_matrix).reshape(N,F,1,H1*stride, W1*tride)
#    
#    #dout_expand # (H1-1)*s, (W1-1)*s (H+2*pad-HH) shape(N,F,1,H,W)
#    dx=np.zeros(N,C,H+2*pad, W+2*pad)
#    
#    x_expand=np.zeros(1,C,H+2*pad, W+2*pad)
#    x_expand=x_expand[N,C,pad:(pad+H),pad:(pad+W)]=x
#    
#    for h in range(HH):
#        for w in range(WW):
#            w_hw=w[:,:,h,w].reshape(1,F,C,1,1)
#            dx_hw=np.sum(w_hw*dout_expand,axis=1)
#            dx[:,:,h:(h+(H1-1)*stride),w:(w+(W1-1)*stride)]+=dx_hw[:,:,h:(h+(H1-1)*stride),w:(w+(W1-1)*stride)]
#                
#            h_range=range(h, h+(H1-1)*stride)
#            w_range=range(w,w+(W1-1)*stride)
#            w_hw= x_expand[:,:,h_range,w_range]
#            
#            dw[:,:,h,w]=np.sum(dout.reshape((N,F,1,H1,W1))*x_hw.reshape((N,1,C,H1,W1)),axis=(0,3,4))
#            
#            db=np.sum(dout, axis=(0,2,3))
#    dx=dx[:,:,pad:(pad+H), pad:(pad+W)]            
#                
    #pass

    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return dx, dw, db


def max_pool_forward_naive(x, pool_param):
    """
    A naive implementation of the forward pass for a max-pooling layer.

    Inputs:
    - x: Input data, of shape (N, C, H, W)
    - pool_param: dictionary with the following keys:
      - 'pool_height': The height of each pooling region
      - 'pool_width': The width of each pooling region
      - 'stride': The distance between adjacent pooling regions

    No padding is necessary here. Output size is given by 

    Returns a tuple of:
    - out: Output data, of shape (N, C, H', W') where H' and W' are given by
      H' = 1 + (H - pool_height) / stride
      W' = 1 + (W - pool_width) / stride
    - cache: (x, pool_param)
    """
    out = None
    ###########################################################################
    # TODO: Implement the max-pooling forward pass                            #
    ###########################################################################
    N,C,H,W=x.shape
    h_pool=pool_param['pool_height']
    w_pool=pool_param['pool_width']
    stride=pool_param['stride']
    
    H1 = int(1 + (H - h_pool) / stride)
    W1 = int(1 + (W - w_pool) / stride)
    
    out=np.zeros((N,C,H1,W1))
    
    #4 loops
    for i in range(N):
        for j in range(C):
            for k in range(H1):
                for l in range(W1):
                    out[i,j,k,l]=\
                    np.max(x[i,j,k*stride:(k*stride+h_pool), l*stride:(l*stride+w_pool)])
                    
    
#    pass
    #2 loops
#    for i in range(H1):
#        for j in range(W1):
#            out[:,:,i,j]=np.max(x[:,:,i*tride:(i*stride+h_pool), j*tride:(j*stride+w_pool)],axis=(2,3))
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    cache = (x, pool_param)
    return out, cache


def max_pool_backward_naive(dout, cache):
    """
    A naive implementation of the backward pass for a max-pooling layer.

    Inputs:
    - dout: Upstream derivatives
    - cache: A tuple of (x, pool_param) as in the forward pass.

    Returns:
    - dx: Gradient with respect to x
    """
    dx = None
    ###########################################################################
    # TODO: Implement the max-pooling backward pass                           #
    ###########################################################################
    x=cache[0]
    N,C,H,W=x.shape
    
    pool_param=cache[1]
    
    h_pool=pool_param['pool_height']
    w_pool=pool_param['pool_width']
    stride=pool_param['stride']
    
    _,_,H1,W1=dout.shape
    
    
    #4 loops
    dx=np.zeros(x.shape)
    for i in range(N):
        for j in range(C):
            for h in range(H1):
                for w in range(W1):
                    spot=x[i,j,h*stride:h*stride+h_pool,w*stride:w*stride+w_pool]
                    dx[i,j,h*stride:h*stride+h_pool,w*stride:w*stride+w_pool]\
                    +=dout[i,j,h,w]*(spot==np.max(spot))
                    
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return dx


def spatial_batchnorm_forward(x, gamma, beta, bn_param):
    """
    Computes the forward pass for spatial batch normalization.

    Inputs:
    - x: Input data of shape (N, C, H, W)
    - gamma: Scale parameter, of shape (C,)
    - beta: Shift parameter, of shape (C,)
    - bn_param: Dictionary with the following keys:
      - mode: 'train' or 'test'; required
      - eps: Constant for numeric stability
      - momentum: Constant for running mean / variance. momentum=0 means that
        old information is discarded completely at every time step, while
        momentum=1 means that new information is never incorporated. The
        default of momentum=0.9 should work well in most situations.
      - running_mean: Array of shape (D,) giving running mean of features
      - running_var Array of shape (D,) giving running variance of features

    Returns a tuple of:
    - out: Output data, of shape (N, C, H, W)
    - cache: Values needed for the backward pass
    """
    out, cache = None, None

    ###########################################################################
    # TODO: Implement the forward pass for spatial batch normalization.       #
    #                                                                         #
    # HINT: You can implement spatial batch normalization by calling the      #
    # vanilla version of batch normalization you implemented above.           #
    # Your implementation should be very short; ours is less than five lines. #
    ###########################################################################
    N, C, H, W=x.shape
    x=x.reshape(N*C, H*W).reshape((C,N*H*W),order='F').T
    out,cache=batchnorm_forward(x, gamma, beta, bn_param)
    out=out.T.reshape((N*C,H*W),order='F').reshape(N,C,H,W)
    
    #pass

    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return out, cache


def spatial_batchnorm_backward(dout, cache):
    """
    Computes the backward pass for spatial batch normalization.

    Inputs:
    - dout: Upstream derivatives, of shape (N, C, H, W)
    - cache: Values from the forward pass

    Returns a tuple of:
    - dx: Gradient with respect to inputs, of shape (N, C, H, W)
    - dgamma: Gradient with respect to scale parameter, of shape (C,)
    - dbeta: Gradient with respect to shift parameter, of shape (C,)
    """
    dx, dgamma, dbeta = None, None, None

    ###########################################################################
    # TODO: Implement the backward pass for spatial batch normalization.      #
    #                                                                         #
    # HINT: You can implement spatial batch normalization by calling the      #
    # vanilla version of batch normalization you implemented above.           #
    # Your implementation should be very short; ours is less than five lines. #
    ###########################################################################
    N,C,H,W=dout.shape
    dout=dout.reshape(N*C, H*W).reshape((C,N*H*W), order='F').T
    dx, dgamma, dbeta=batchnorm_backward(dout, cache)
    dx=dx.T.reshape((N*C,H*W), order='F').reshape(N,C,H,W)
    
#    pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return dx, dgamma, dbeta





def spatial_groupnorm_forward(x, gamma, beta, G, gn_param):
    """
    Computes the forward pass for spatial group normalization.
    In contrast to layer normalization, group normalization splits each entry 
    in the data into G contiguous pieces, which it then normalizes independently.
    Per feature shifting and scaling are then applied to the data, in a manner 
    identical to that of batch normalization and layer normalization.

    Inputs:
    - x: Input data of shape (N, C, H, W)
    - gamma: Scale parameter, of shape (C,)
    - beta: Shift parameter, of shape (C,)
    - G: Integer mumber of groups to split into, should be a divisor of C
    - gn_param: Dictionary with the following keys:
      - eps: Constant for numeric stability

    Returns a tuple of:
    - out: Output data, of shape (N, C, H, W)
    - cache: Values needed for the backward pass
    """
    out, cache = None, None
    eps = gn_param.get('eps',1e-5)
    ###########################################################################
    # TODO: Implement the forward pass for spatial group normalization.       #
    # This will be extremely similar to the layer norm implementation.        #
    # In particular, think about how you could transform the matrix so that   #
    # the bulk of the code is similar to both train-time batch normalization  #
    # and layer normalization!                                                # 
    ###########################################################################
    N,C,H,W=x.shape
    
    xs=np.split(x,G,axis=1)
    
    dim_group=int(C/G)

    
    xs=[xs[i] for i in range(G)]#G,N,C/G,H,W
    xs=np.array(xs).reshape(G*N, dim_group*H*W) # G,N,*
    xs=xs.T
    xs_mean=np.mean(xs,axis=0)
    xs_var=np.var(xs,axis=0)
    xs_norm=(xs-xs_mean)/np.sqrt(xs_var+eps)
    
    xs_norm=xs_norm.T
    xs_norm=xs_norm.reshape(G,N,dim_group,H,W)
    xs_norm=xs_norm.transpose(1,0,2,3,4)
    xs_norm=xs_norm.reshape(N,C,H,W)
    out=xs_norm*gamma+beta
    
    cache=(xs_norm,xs_var, eps, xs, xs_mean, gamma, G)  
    
    #pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return out, cache



def spatial_groupnorm_backward(dout, cache):
    """
    Computes the backward pass for spatial group normalization.

    Inputs:
    - dout: Upstream derivatives, of shape (N, C, H, W)
    - cache: Values from the forward pass

    Returns a tuple of:
    - dx: Gradient with respect to inputs, of shape (N, C, H, W)
    - dgamma: Gradient with respect to scale parameter, of shape (C,)
    - dbeta: Gradient with respect to shift parameter, of shape (C,)
    """
    dx, dgamma, dbeta = None, None, None

    ###########################################################################
    # TODO: Implement the backward pass for spatial group normalization.      #
    # This will be extremely similar to the layer norm implementation.        #
    ###########################################################################
    
    xs_norm, xs_var, eps, xs, xs_mean, gamma, G =cache
    N, C, H, W=dout.shape
    
    dgamma=np.sum(dout*xs_norm, axis=(0,2,3)).reshape(1,C,1,1)

    dbeta=np.sum(dout, axis=(0,2,3)).reshape(1,C,1,1)
    
    dim_group=int(C/G)
    
    dxs_norm=(dout*gamma).reshape(N,G,dim_group,H,W).transpose(1,0,2,3,4).reshape(N*G,dim_group*H*W)
    
    M,D=dxs_norm.shape
    
    dxs_norm=dxs_norm.T
    dx1=dxs_norm/np.sqrt(xs_var+eps)
    dx2=-(1/D)*np.sum(dxs_norm, axis=0)/np.sqrt(xs_var+eps)
    dx3=-(1/D)*np.sum(dxs_norm*(xs-xs_mean), axis=0)/np.power(xs_var+eps, 3/2)*(xs-xs_mean)
    
    dx=dx1+dx2+dx3
    
    dx=dx.T
    dx=dx.reshape(G,N,dim_group,H,W).transpose(1,0,2,3,4).reshape(N,C,H,W)
    
    pass
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################
    return dx, dgamma, dbeta


def svm_loss(x, y):
    """
    Computes the loss and gradient using for multiclass SVM classification.

    Inputs:
    - x: Input data, of shape (N, C) where x[i, j] is the score for the jth
      class for the ith input.
    - y: Vector of labels, of shape (N,) where y[i] is the label for x[i] and
      0 <= y[i] < C

    Returns a tuple of:
    - loss: Scalar giving the loss
    - dx: Gradient of the loss with respect to x
    """
    N = x.shape[0]
    correct_class_scores = x[np.arange(N), y]
    margins = np.maximum(0, x - correct_class_scores[:, np.newaxis] + 1.0)
    margins[np.arange(N), y] = 0
    loss = np.sum(margins) / N
    num_pos = np.sum(margins > 0, axis=1)
    dx = np.zeros_like(x)
    dx[margins > 0] = 1
    dx[np.arange(N), y] -= num_pos
    dx /= N
    return loss, dx


def softmax_loss(x, y):
    """
    Computes the loss and gradient for softmax classification.

    Inputs:
    - x: Input data, of shape (N, C) where x[i, j] is the score for the jth
      class for the ith input.
    - y: Vector of labels, of shape (N,) where y[i] is the label for x[i] and
      0 <= y[i] < C

    Returns a tuple of:
    - loss: Scalar giving the loss
    - dx: Gradient of the loss with respect to x
    """
    shifted_logits = x - np.max(x, axis=1, keepdims=True)
    Z = np.sum(np.exp(shifted_logits), axis=1, keepdims=True)
    log_probs = shifted_logits - np.log(Z)
    probs = np.exp(log_probs)
    N = x.shape[0]
    loss = -np.sum(log_probs[np.arange(N), y]) / N
    dx = probs.copy()
    dx[np.arange(N), y] -= 1
    dx /= N
    return loss, dx
