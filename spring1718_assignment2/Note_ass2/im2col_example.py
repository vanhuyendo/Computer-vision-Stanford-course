#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 11:53:36 2018

@author: huyen
"""
import numpy as np

# Example for vectorizing a matrix 4*4 to adapt with a convolutional matrix 2*2 and stride 1.

# input: x: matrix 4*4

#output:matrix (3*3)*(2*2)

x=np.arange(16).reshape(4,4)

W,H=x.shape

HH=2
WW=2
S=1

H1=int((H-HH)/S+1)
W1=int((W-WW)/S+1)

indx_00=S*np.arange(H1).reshape(1,-1)
indx_01=np.arange(HH).reshape(-1,1)


indx0=indx_00+indx_01
indx0=indx0.reshape(H1,1,HH,1).astype(int)

indx_10=S*np.arange(W1).reshape(-1,1)
indx_11=np.arange(WW).reshape(1,-1)


indx1=indx_10+indx_11
indx1=indx1.reshape(1,W1,1,WW).astype(int)



y=x[indx0,indx1]

y.reshape(H1*W1, HH*WW)

# example for vectorizing a matrix 4*4*2 to adapt with a convolutional matrix 2*2*2 a stride 1

#  input x: 4*4*2
# output: matrix (3*3)*(2*2*2)

x=np.arange(32).reshape(4,4,2)

W,H,C=x.shape

HH=2
WW=2
S=1

H1=int((H-HH)/S+1)
W1=int((W-WW)/S+1)

indx_00=S*np.arange(H1).reshape(1,-1)
indx_01=np.arange(HH).reshape(-1,1)


indx0=indx_00+indx_01
indx0=indx0.reshape(H1,1,HH,1,1).astype(int)

indx_10=S*np.arange(W1).reshape(-1,1)
indx_11=np.arange(WW).reshape(1,-1)


indx1=indx_10+indx_11
indx1=indx1.reshape(1,W1,1,WW,1).astype(int)

indx2=np.arange(C).reshape(1,1,1,1,C)

y=x[indx0,indx1,indx2]

#without playing with C
ind0=indx0.reshape(H1,1,HH,1)
ind1=indx1.reshape(1,W1,1,WW)

z=x[ind0,ind1,:]



