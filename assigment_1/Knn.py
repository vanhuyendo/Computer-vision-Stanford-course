# -*- coding: utf-8 -*-
"""
Created on Wed Apr 25 15:41:00 2018

@author: huyen
"""
#load batch 1
import os

os.chdir('/home/huyen/course/computer_vision_oxford/assigment_1/cifar-10-batches-py')

# import data with pickle and return a dictionary

file= '/home/huyen/course/computer_vision_oxford/assigment_1/cifar-10-batches-py/data_batch_1'

def unpickle(file):
    import cPickle
    with open(file, 'rb') as fo:
        dict = cPickle.load(fo)
    return dict

file1=unpickle(file)

type(file1)
len(file1)#4
file1.keys()#['data', 'labels', 'batch_label', 'filenames']
len(file1['labels'])#list of 10000 labels 0:9
file1['data'].shape#np.array 10000*3072 3072=32*32*3=image

#show some images

import matplotlib.pyplot as plt

plt.rcParams['figure.figsize'] = (10.0, 8.0) # set default size of plots
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

images=file1['data']

im=images[3]
plt.imshow(im.reshape(3,32,32).transpose([1, 2, 0]))
file1['labels'][3]

len(file1['filenames'])#10000 names of each image

file1['filenames'][3]


# Show some random photos with its labels
import numpy as np

X=images
Y=np.array(file1['labels'])

classes = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
n_sample=7
n_class=len(classes)

for y, cls in enumerate(classes):
    idxs=np.flatnonzero(Y==y)
    idxs=np.random.choice(idxs, n_sample,replace=False)
    for i, idx in enumerate(idxs):
        plt_idx=i*n_class+y+1
        plt.subplot(n_sample, n_class, plt_idx)
        plt.imshow(X[idx].reshape(3,32,32).transpose([1, 2, 0]))
        plt.axis('off')
        if i==0:
            plt.title(cls)
plt.show()
        
# Pick randomly 8000 samples for training set and the rest for test set.
        
n_data=len(X)        
n_test=2000
idx_test=np.random.choice(range(n_data),n_test,replace=False)
idx_train=np.array(list(set(range(n_data))-set(idx_test)))
X_test=X[idx_test]
Y_test=Y[idx_test]
X_train=X[idx_train]
Y_train=Y[idx_train]        
 
 
#Check if order of training set is correct
for y, cls in enumerate(classes):
    idxs=np.flatnonzero(Y_train==y)
    idxs=np.random.choice(idxs, n_sample,replace=False)
    for i, idx in enumerate(idxs):
        plt_idx=i*n_class+y+1
        plt.subplot(n_sample, n_class, plt_idx)
        plt.imshow(X_train[idx].reshape(3,32,32).transpose([1, 2, 0]))
        plt.axis('off')
        if i==0:
            plt.title(cls)
plt.show()


########################################## 
# coding the k_nearest neighborhood function


###########
def distance(a,b):
    '''Euclidean distance between two images
    '''
    return np.sqrt(np.sum((a-b)*(a-b)))
    
##########
def K_nearest(a,ims,labels, K=5):
    '''Pick K nearest images to a from a set of images. Return indexes and distances and labels.
    ''' 
    assert len(ims)==len(labels)
    assert len(ims)>=K
    n=len(ims)
    dtype=[('index',int),('distance',float),('labels',int)]
    values=[(i, distance(a, ims[i]), labels[i]) for i in range(n)]
    ds=np.array(values, dtype=dtype)
    ds=np.sort(ds, order='distance')
    return ds[:K]
    
    
ims=np.array([[1,9],[3,5],[2,1],[2,3],[0,2],[0,4]])
a=np.array([[0,0]])
labs=np.array([1,1,1,0,0,0]).transpose()
K_nearest(a,ims,labs, K=2)
    
    
######## case_study_kNN
def K_nearest_list(a,ims,K=5):
    '''Pick K nearest images to a from a set of images. Return indexes and distances and labels.
    ''' 
    distances={}
    for i in range(len(ims)):
        distances[i]=distance(a,ims[i])
    distances_sorted=sorted(distances.items(), key=lambda x:x[1])
    return distances_sorted[:K]


dis_k=K_nearest_list(a, ims, K=2)    

######### case_study_kNN
def labels_k(distances_k, labels):
    labels_k=[labels[distances_k[i][0]] for i in range(len(distances_k))]
    return labels_k
    
    
###################    

def mode(arr):
    '''Find mode.
    '''    
    uniques, counts = np.unique(arr, return_counts=True)
    mode=max(counts)
    for i in range(len(counts)):
        if counts[i]==mode:
            return uniques[i]
            
    
#    
a = np.array([0, 3, 0, 1, 0, 1, 2, 1, 0, 0, 0, 0, 1, 3, 4])
uniques, counts = np.unique(a, return_counts=True)    

mode(a)

##########
def Knn(a,images, labels, K):
    ''' Classify an image by Knn method.
    '''
    neighbors=K_nearest(a, images, labels, K)
    return mode(neighbors['labels'])
    

#

a=np.array([[0,0]])
images=np.array([[1,2],[4,5],[2,1],[1,1],[0,4],[0,1]])
labels=np.array([[0,1,0,1,1,1]]).transpose()

Knn(a,ims,labs,K=3)
#########
def error(labels,predictions):
    '''Calculate the proportion of correct predictions.
    '''    
    assert len(labels)==len(predictions)
    correct=np.flatnonzero(labels-predictions)
    return 1.0*len(correct)/len(labels)


labs=np.array([[0,1,2,1,2,0]]).transpose()
preds=np.array([[0,1,0,2,2,2]]).transpose()

error(labs, preds)
    
#########
def test_error(im_test, la_test, im_train, la_train, K):
    ''' Calculate proportion of correct predictions in terms of K.
    '''    
    pred_test=np.array([Knn(im_test[i], im_train, la_train, K) for i in range(len(im_test))]).reshape([-1,1])
    return error(la_test, pred_test)


a=np.array([[0,0],[3,2]])
b=np.array([[0],[0]])
images=np.array([[1,2],[4,5],[2,1],[1,1],[0,4],[0,1]])
labels=np.array([[0,1,0,1,1,1]]).transpose()

im_test=a
la_test=b
im_train=images
la_train=labels

test_error(a,b,images,labels,K=4)
test_penalty(a,b,images,labels,K=4,penat=0.1)
########
def test_penalty(im_test, la_test, im_train, la_train, penat, K):  
    ''' Calculate proportion of correct predictions in terms of K with penalty on K.
    ''' 
    return test_error(im_test, la_test, im_train, la_train, K)+ penat*K
    
########   
def errors_test(im_test, la_test, im_train, la_train, penat, thres, K_ini=5, K_max=20):
    ''' Record errors in terms of K until it increases repeatly some threshold steps. 
    '''
    assert K_max<= len(im_train)
    table_error=np.empty([0,2])
    increase=0
    table_error=np.append(table_error, [[K_ini,  test_penalty(im_test, la_test, im_train, \
    la_train, penat, K_ini)]],axis=0)
    while increase < thres and K_ini<=K_max:
        print(K_ini)
        K_ini+=1
        error=test_penalty(im_test, la_test, im_train, la_train, penat, K_ini)
        if error >=table_error[-1,1]:
            increase+=1
        else:
            increase=0
        table_error=np.append(table_error, [[K_ini, error]],axis=0)
        print(table_error)
    return table_error



im_test=a
la_test=b
im_train=images
la_train=labels
K_ini=1
K_max=3
penat=0.01
thres=3

test_penalty(a,b,images, labels,K=3,penat=0.01)
test_error(a,b,images, labels,K=3)



errors_test(a,b,images,labels,K_ini=1,penat=0.1,K_max=5,thres=3)

#########
#visualize the errors

table=errors_test(im_test, la_test, im_train, la_train, penat, thres, K_ini=1, K_max=5)
plt.plot(table[:,0], table[:,1], 'ro-')

    
########
def folds(array,n_folds=5):
    '''Divide the train set into K equal-subsets.
    '''
    array_split=np.array_split(array,n_folds)
    array_folds={}
    for i in range(n_folds):
        array_folds[i]=array_split[i]
    return array_folds


images
folds(images,2)
##########

def error_folds(x_train, y_train,n_folds,n_neighbors):
    ''' Cross-validation error.
    '''
    x_folds=folds(x_train, n_folds)
    y_folds=folds(y_train, n_folds)
    error=np.empty([n_folds])
    col=x_train.shape[-1]
    for i in range(n_folds):
        im_test=x_folds[i]
        la_test=y_folds[i]
        im_train=np.empty([0,col])
        la_train=np.empty([0,1])
        for j in range(n_folds):
            if j!=i:
                im_train=np.concatenate((im_train, x_folds[j]),axis=0)
                la_train=np.concatenate((la_train,y_folds[j]),axis=0)
        error[i]=test_error(im_test, la_test, im_train, la_train, n_neighbors)
    return error

images
labels

x_train=images
y_train=labels
n_folds=2
n_neighbors=3       
        
error_folds(images, labels,2,3)        

######################### 
############## To copy from class 

from cs231n.classifiers import KNearestNeighbor
from cs231n.data_utils import load_CIFAR10
        
        