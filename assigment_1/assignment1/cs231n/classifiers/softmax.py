import numpy as np
from random import shuffle
from past.builtins import xrange

def softmax_loss_naive(W, X, y, reg):
  """
  Softmax loss function, naive implementation (with loops)

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)


  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  n_sample=X.shape[0]
  n_class=W.shape[1]
  for i in range(n_sample):
      score=X[i].dot(W)
      exp_score=np.exp(score)
      loss_numerator=np.sum(exp_score)
      loss_exp=exp_score/loss_numerator
      loss += -score[y[i]]+ np.log(loss_numerator)
      for k in range(n_class):
          if k==y[i]:
              dW[:,k]+= (-1+(exp_score[k]/loss_numerator))*X[i]
          else:
              dW[:,k]+= (exp_score[k]/loss_numerator)*X[i]
#  print(exp_score.shape, loss_numerator.shape, loss_exp.shape)
  loss=loss/n_sample+reg*np.sum(W*W)
  dW=dW/n_sample+reg*2*W
  #pass
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
  """
  Softmax loss function, vectorized version.

  Inputs and outputs are the same as softmax_loss_naive.
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  
  S=X.dot(W) #N*C
  n_sample =X.shape[0]
  n_class=S.shape[1]
  Denominator=np.sum(np.exp(S),axis=1) #1*N
  class_repeat=np.repeat(range(n_class),n_sample).reshape((n_sample,n_class), order='F')
  y_repeat=np.repeat(y,n_class).reshape((n_sample,n_class), order='C')
  S_indx=class_repeat==y_repeat
  loss_1=-np.sum(S*S_indx)
  loss_2=np.sum(np.log(Denominator)) #1*N
  loss=(loss_1+loss_2)/n_sample +reg*np.sum(W*W)
  dS=-1*S_indx+ np.exp(S)/Denominator.reshape(n_sample,1)
  dW=np.transpose(X).dot(dS)/n_sample+reg*2*W
    #pass
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW

